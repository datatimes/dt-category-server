""" testing categories.py """
 # pylint: disable=protected-access, unidiomatic-typecheck
import os
from unittest.mock import mock_open, patch
from tdatim_category_server.categories import strip_document, _load_data_times_categories
import tdatim_category_server.categories


def test_strip_document():
    ''' returns a list '''
    tdatim_category_server.categories._STOP_WORDS = []
    doc = 'does this need to be a sentence about sports, or crime or windows'
    output = strip_document(doc)
    assert output is not None
    assert type(output) is list


def test_load_data_times_categories():
    # mock the open command
    ''' returns a sorted dictionary & uses dtcat_filename '''
    mock = mock_open()
    with patch('tdatim_category_server.categories.open', mock, create=True):
        doc = os.path.join('/data', 'dtcats.md')
        output = _load_data_times_categories(doc)
        print(output)
        assert output is not None
