""" module that finds categories in a document,
based on the wikipedia corpus """
# pylint: disable=anomalous-backslash-in-string, global-statement, unidiomatic-typecheck, redefined-argument-from-local
import os
import re
# from gensim.models import FastText as ft
from gensim.models import KeyedVectors
import nltk
from nltk.corpus import stopwords
from sortedcontainers import SortedDict
import numpy as np

THRESHOLD = 0.1

_STOP_WORDS = None
_MODEL = None
_CLASSIFIER_BOW = None
_TOPIC_VECTORS = None

RE_SW = re.compile(r'\b\w\b')
RE_WS = re.compile(r'\s+')
RE_NUM = re.compile('[^A-Za-z\s]')

DATA_LOCATION = os.environ['TDATIM_DATA'] if 'TDATIM_DATA' in os.environ else '/data'


def load():
    """ Load the wikipedia corpus """
    global _STOP_WORDS, _MODEL, _CLASSIFIER_BOW, _TOPIC_VECTORS

    if not _STOP_WORDS:
        nltk.data.path.append(DATA_LOCATION)
        _STOP_WORDS = stopwords.words('english')
    if not _MODEL:
        print('LOADING MODEL')
        #_model = ft.load_fasttext_format('/data/wiki.en.bin')
        _MODEL = KeyedVectors.load(DATA_LOCATION + "/fasttext_small.model")
        print('LOADED MODEL')
    if not _CLASSIFIER_BOW:
        _CLASSIFIER_BOW = _load_data_times_categories(
            DATA_LOCATION + '/dtcats.md')
        _TOPIC_VECTORS = [
            (np.mean(2 * [_MODEL.wv.get_vector(k[0])]
                     + [_MODEL.wv.get_vector(w) for w in l],
                     axis=0), [k[0]] + l) for k, l in _CLASSIFIER_BOW.items()
        ]


def strip_document(doc):
    """ Breaks the input down into a list of words that can be compared with the corpus (?) """
    global _STOP_WORDS

    if type(doc) is list:
        doc = ' '.join(doc)

    docs = doc.split(',')
    # raise Exception(docs, 'oh dear')
    word_list = []
    for doc in docs:
        print(doc)
        doc = doc.replace('\n', ' ').replace('_', ' ').lower()
        doc = RE_WS.sub(' ', RE_SW.sub('', RE_NUM.sub('', doc))).strip()

        if doc == '':
            word_list.append([])
        print(doc)
        # generator is creating a list
        # printing each word in the document if it's not in _stop_words,
        # using each word as an item in the list
        word_list.append([w for w in doc.split(' ') if w not in _STOP_WORDS])

    return word_list


def _load_data_times_categories(dtcat_filename):
    classifier_bow = SortedDict([])

    with open(dtcat_filename, 'r') as cat_f:
        category = ''
        for line in (l.strip().lower() for l in cat_f.readlines()):
            if not line:
                continue

            if line.startswith('[') and line.endswith(']'):
                category = line[1:-1]
                continue

            if ':' in line:
                label, words = line.split(':')
                words = words.split(',')
            else:
                label, words = (line, [line])

            classifier_bow[(category, label)] = [category] + words
    return classifier_bow


def test(sentence):
    """ Function uses the stripped down document to find categories """
    global _CLASSIFIER_BOW, _TOPIC_VECTORS, _MODEL

    clean = strip_document(sentence)

    if not clean:
        return []

    tags = set()
    for words in clean:
        if not words:
            continue

        vec = np.mean([_MODEL.wv.get_vector(w) for w in words], axis=0)
        result = _MODEL.wv.cosine_similarities(
            vec, [t for t, _ in _TOPIC_VECTORS])
        # result = sorted([(t, model.wv.n_similarity(t, clean))
        # for t in topics], key=lambda x: x[1], reverse=False)

        top = np.nonzero(result > THRESHOLD)[0]

        tags.update({(result[i], _CLASSIFIER_BOW.keys()[i]) for i in top})
    return tags
