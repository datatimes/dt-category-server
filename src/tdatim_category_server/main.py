""" Runs the category checker """
# pylint: disable=undefined-variable, unidiomatic-typecheck
from . import categories_new as categories
import sys
from . import categories


def run(highlights, score):
    """
    Execute category server on the command line, based on STDIN
    """
    cm = categories.CategoryManager()
    cm.load()
    for data in sys.stdin:
        if data.startswith('[cr]'):
            cm._categories['dtcats'] = None
            cm.load()

        data = data.split(';')

        if not data or type(data) is not list:
            abort(
                400, _("Must have a list of strings as posted JSON with text to categorize"))

        matches = cm.test(data)
        result = [(float(r), k) for r, k in matches]
        if highlights or score:
            result = sorted(filter(lambda x: x[0] > 0.25, result), key=lambda x: x[0], reverse=True)[0:4]
            if score:
                result = f'{result[0][1][0]: <20}| {data}'
        yield result

if __name__ == "__main__":
    highlights = (sys.argv[1] == '--highlights') if len(sys.argv) > 1 else False
    score = (sys.argv[1] == '--score') if len(sys.argv) > 1 else False
    for output in run(highlights, score):
        print(output)
