#!/bin/sh

kubectl config use-context bfc.dni-dev

kubectl scale --replicas=0 statefulset/tdatim-category-server

sleep 20

kubectl delete pod tdatim-category-server

kubectl create -f helm-tdatim-update.yaml

sleep 20

kubectl exec -ti tdatim-category-server -- mv /data/dtcats.md /data/dtcats-2019-07-13.md
kubectl cp data/dtcats.md tdatim-category-server:/data/dtcats.md

kubectl exec -ti tdatim-category-server -- cat /data/dtcats.md

kubectl delete pod tdatim-category-server

sleep 20

kubectl scale --replicas=1 statefulset/tdatim-category-server
