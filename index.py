#!/usr/bin/env python
'''
Copyright (c) 2015 Jesse Peterson
Licensed under the MIT license. See the included LICENSE.txt file for details.
Copyright (c) Alex Ellis 2017. All rights reserved.
Licensed under the MIT license. See LICENSE file in the project root for full license information.
'''

import logging
from flask import request
from flask import Flask, redirect
from flask_restful import Api
from tdatim_category_server import categories_new as categories
import os
import handler


def create_app(): # This function is used for creating an app object. Will return an object of type app.
    app = Flask(__name__) # Creating application object, standard for setting up Flask
    api = Api(app) # Setting up the Flask API

    api.add_resource(handler.Handler, '/')  #Add resource to the API?

    category_manager = categories.make_category_manager()
    handler.Handler.set_category_manager(category_manager)

    app.logger.error("Preload beginning.")
    if not handler.Handler.preload():  #Call load function from result_service object
        raise RuntimeError("Preload returned false")

    if 'TDATIM_CATEGORIES_CAN_REGISTER' in os.environ and os.environ['TDATIM_CATEGORIES_CAN_REGISTER']:
        api.add_resource(handler.CategoriesHandler, '/categories')  #Add resource to the API?
        handler.CategoriesHandler.set_category_manager(category_manager)

    app.logger.error("Preload done.")

    return app      #return app object

def setup(): # Set up function
    app = create_app()  # Creating an app object
    app.logger.addHandler(logging.StreamHandler()) # Adding handler/logger to app
    app.logger.setLevel(logging.INFO) # Set level of logger to INFO
    return app # return app object


app = setup() # call setup method on app object

@app.before_request
def fix_transfer_encoding():
    """
    Sets the "wsgi.input_terminated" environment flag, thus enabling
    Werkzeug to pass chunked requests as streams.  The gunicorn server
    should set this, but it's not yet been implemented.
    """

    transfer_encoding = request.headers.get("Transfer-Encoding", None)
    if transfer_encoding == u"chunked":
        request.environ["wsgi.input_terminated"] = True

if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0')
